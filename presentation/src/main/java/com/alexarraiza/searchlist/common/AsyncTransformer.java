package com.alexarraiza.searchlist.common;

import com.alexarraiza.domain.common.Transformer;

import io.reactivex.Observable;
import io.reactivex.ObservableSource;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

public class AsyncTransformer<T> extends Transformer<T> {
	@Override
	public ObservableSource<T> apply(Observable<T> upstream) {
		return upstream.subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread());
	}
}
