package com.alexarraiza.searchlist.common;

import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;

public abstract class AbstractPresenter {

	private CompositeDisposable compositeDisposable = new CompositeDisposable();

	protected void addDisposable(Disposable disposable){
		compositeDisposable.add(disposable);
	}

	protected void clearDisposables() {
		compositeDisposable.clear();
	}

}
