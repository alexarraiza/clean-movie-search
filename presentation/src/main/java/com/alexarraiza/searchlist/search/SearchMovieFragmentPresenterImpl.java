package com.alexarraiza.searchlist.search;

import com.alexarraiza.data.entities.MovieData;
import com.alexarraiza.data.mappers.MovieEntityDataMapper;
import com.alexarraiza.domain.MoviesRepository;
import com.alexarraiza.domain.common.Mapper;
import com.alexarraiza.domain.entities.MovieEntity;
import com.alexarraiza.domain.usecases.SearchMovieUseCase;
import com.alexarraiza.searchlist.common.AbstractPresenter;
import com.alexarraiza.searchlist.common.AsyncTransformer;

import java.util.List;

import io.reactivex.ObservableSource;
import io.reactivex.functions.Consumer;
import io.reactivex.functions.Function;

public class SearchMovieFragmentPresenterImpl extends AbstractPresenter implements SearchMovieFragmentPresenter {

	private AsyncTransformer asyncTransformerMovie;
	private Mapper<MovieEntity, MovieData> entityDataMapper;
	private MoviesRepository moviesRepository;
	private SearchMovieFragmentPresenter.BaseView mView;

	public SearchMovieFragmentPresenterImpl(AsyncTransformer asyncTransformerMovie, MovieEntityDataMapper entityDataMapper, MoviesRepository moviesRepository, BaseView mView) {
		this.asyncTransformerMovie = asyncTransformerMovie;
		this.entityDataMapper = entityDataMapper;
		this.moviesRepository = moviesRepository;
		this.mView = mView;
	}

	@Override
	public void performSearch(String query) {
		SearchMovieUseCase searchMovieUseCase = new SearchMovieUseCase(asyncTransformerMovie, moviesRepository);
		addDisposable(searchMovieUseCase.search(query)
				.flatMap(new Function<List<MovieEntity>, ObservableSource<?>>() {
					@Override
					public ObservableSource<List<MovieData>> apply(List<MovieEntity> movieEntities) {
						return entityDataMapper.observable(movieEntities);
					}
				})
				.subscribe(new Consumer() {
					@Override
					public void accept(Object o) throws Exception {
						if (o instanceof List) {
							mView.showSearchResults((List<MovieData>) o);
						}
					}
				}, new Consumer<Throwable>() {
					@Override
					public void accept(Throwable throwable) {

					}
				}));
	}

	@Override
	public void onDestroy() {
		clearDisposables();
	}
}
