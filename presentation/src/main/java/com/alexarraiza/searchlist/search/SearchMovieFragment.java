package com.alexarraiza.searchlist.search;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.alexarraiza.data.entities.MovieData;
import com.alexarraiza.data.mappers.MovieEntityDataMapper;
import com.alexarraiza.data.repositories.MoviesRepositoryImpl;
import com.alexarraiza.searchlist.R;
import com.alexarraiza.searchlist.common.AsyncTransformer;

import java.util.List;

import butterknife.ButterKnife;

public class SearchMovieFragment extends Fragment implements SearchMovieFragmentPresenter.BaseView {

	private static final String TAG = "SearchMovieFragment";

	private SearchMovieFragmentPresenter mPresenter;

	@Nullable
	@Override
	public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
		View v = inflater.inflate(R.layout.fragment_search_movie, null);
		ButterKnife.bind(this, v);
		return v;
	}

	@Override
	public void onActivityCreated(@Nullable Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);

		mPresenter = new SearchMovieFragmentPresenterImpl(
				new AsyncTransformer(),
				new MovieEntityDataMapper(),
				new MoviesRepositoryImpl(),
				this
		);

		mPresenter.performSearch("test");
	}

	@Override
	public void showSearchResults(List<MovieData> movieDataList) {

		Log.i(TAG, "showSearchResults: " + movieDataList.size());

	}

	@Override
	public void onDestroy() {
		super.onDestroy();
		mPresenter.onDestroy();
	}
}
