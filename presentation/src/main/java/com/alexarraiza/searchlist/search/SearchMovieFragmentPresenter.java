package com.alexarraiza.searchlist.search;

import com.alexarraiza.data.entities.MovieData;
import com.alexarraiza.searchlist.common.BasePresenter;

import java.util.List;

public interface SearchMovieFragmentPresenter extends BasePresenter {

	void performSearch(String query);

	interface BaseView {

		void showSearchResults(List<MovieData> movieDataList);

	}

}
