package com.alexarraiza.searchlist.ui.activities;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

import com.alexarraiza.searchlist.R;
import com.alexarraiza.searchlist.search.SearchMovieFragment;

import butterknife.ButterKnife;

public class MainActivity extends AppCompatActivity {

	@Override
	protected void onCreate(@Nullable Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		ButterKnife.bind(this);

		getSupportFragmentManager()
				.beginTransaction()
				.replace(R.id.frame, new SearchMovieFragment())
				.commitNow();

	}
}
