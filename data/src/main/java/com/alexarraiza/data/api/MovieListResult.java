package com.alexarraiza.data.api;

import com.alexarraiza.data.entities.MovieData;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class MovieListResult {

	@SerializedName("page") private int page;
	@SerializedName("results") private List<MovieData> movies;

	public MovieListResult(int page, List<MovieData> movies) {
		this.page = page;
		this.movies = movies;
	}

	public int getPage() {
		return page;
	}

	public void setPage(int page) {
		this.page = page;
	}

	public List<MovieData> getMovies() {
		return movies;
	}

	public void setMovies(List<MovieData> movies) {
		this.movies = movies;
	}
}
