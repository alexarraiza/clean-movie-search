package com.alexarraiza.data.api;

import io.reactivex.Observable;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface Api {

	@GET("search/movie")
	Observable<MovieListResult> searchMovies(@Query("query") String query);

}
