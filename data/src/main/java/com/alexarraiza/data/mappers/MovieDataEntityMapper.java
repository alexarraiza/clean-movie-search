package com.alexarraiza.data.mappers;

import com.alexarraiza.data.entities.MovieData;
import com.alexarraiza.domain.common.Mapper;
import com.alexarraiza.domain.entities.MovieEntity;

public class MovieDataEntityMapper extends Mapper<MovieData, MovieEntity> {
	@Override
	public MovieEntity mapFrom(MovieData from) {
		MovieEntity movieEntity = new MovieEntity();
		movieEntity.setId(from.getId());
		movieEntity.setTitle(from.getTitle());
		movieEntity.setOverview(from.getOverview());
		movieEntity.setVoteCount(from.getVoteCount());
		movieEntity.setPosterPath(from.getPosterPath());
		movieEntity.setBackdropPath(from.getBackdropPath());

		return movieEntity;
	}
}
