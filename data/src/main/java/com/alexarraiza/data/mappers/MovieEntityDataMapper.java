package com.alexarraiza.data.mappers;

import com.alexarraiza.data.entities.MovieData;
import com.alexarraiza.domain.common.Mapper;
import com.alexarraiza.domain.entities.MovieEntity;

public class MovieEntityDataMapper extends Mapper<MovieEntity, MovieData> {
	@Override
	public MovieData mapFrom(MovieEntity from) {
		MovieData movieData = new MovieData();
		movieData.setId(from.getId());
		movieData.setTitle(from.getTitle());
		movieData.setOverview(from.getOverview());
		movieData.setVoteCount(from.getVoteCount());
		movieData.setPosterPath(from.getPosterPath());
		movieData.setBackdropPath(from.getBackdropPath());

		return movieData;
	}
}
