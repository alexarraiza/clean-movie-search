package com.alexarraiza.data.repositories;

import com.alexarraiza.domain.MoviesRepository;
import com.alexarraiza.domain.common.DomainTestUtils;
import com.alexarraiza.domain.entities.MovieEntity;

import java.util.List;

import io.reactivex.Observable;

public class MoviesRepositoryImpl implements MoviesRepository {

	/**
	 * Gets movies from a search query. Right now returns *stubbed* data
	 * @param searchQuery Search term to use for comparison
	 * @return List of movies that match the search query
	 */
	@Override
	public Observable<List<MovieEntity>> searchMovie(String searchQuery) {
		return Observable.just(DomainTestUtils.generateMovieEntityList());
	}
}
