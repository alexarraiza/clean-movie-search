package com.alexarraiza.data.entities;

import com.google.gson.annotations.SerializedName;

public class MovieData {

	@SerializedName("id") private int id;
	@SerializedName("title") private String title;
	@SerializedName("overview") private String overview;
	@SerializedName("vote_count") private int voteCount;
	@SerializedName("poster_path") private String posterPath;
	@SerializedName("backdrop_path") private String backdropPath;

	public MovieData() {
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getOverview() {
		return overview;
	}

	public void setOverview(String overview) {
		this.overview = overview;
	}

	public int getVoteCount() {
		return voteCount;
	}

	public void setVoteCount(int voteCount) {
		this.voteCount = voteCount;
	}

	public String getPosterPath() {
		return posterPath;
	}

	public void setPosterPath(String posterPath) {
		this.posterPath = posterPath;
	}

	public String getBackdropPath() {
		return backdropPath;
	}

	public void setBackdropPath(String backdropPath) {
		this.backdropPath = backdropPath;
	}
}
