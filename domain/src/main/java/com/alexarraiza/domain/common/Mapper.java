package com.alexarraiza.domain.common;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;

import io.reactivex.Observable;

public abstract class Mapper<E, T> {

	public abstract T mapFrom(E from);

	public Observable<T> observable(final E from) {
		return Observable.fromCallable(new Callable<T>() {
			@Override
			public T call() {
				return mapFrom(from);
			}
		});
	}

	public Observable<List<T>> observable(final List<E> from) {
		return Observable.fromCallable(new Callable<List<T>>() {
			@Override
			public List<T> call() {

				List<T> toList = new ArrayList<>();

				for (E fromItem : from) {
					toList.add(mapFrom(fromItem));
				}

				return toList;

			}
		});
	}

}
