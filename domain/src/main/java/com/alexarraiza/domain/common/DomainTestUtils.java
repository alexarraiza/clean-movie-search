package com.alexarraiza.domain.common;

import com.alexarraiza.domain.entities.MovieEntity;

import java.util.ArrayList;
import java.util.List;

public abstract class DomainTestUtils {

	public static MovieEntity getTestMovieEntity(int id) {
		MovieEntity movieEntity = new MovieEntity();
		movieEntity.setId(id);
		movieEntity.setTitle("Movie" + String.valueOf(id));
		return movieEntity;
	}

	public static List<MovieEntity> generateMovieEntityList() {
		List<MovieEntity> movieEntityList = new ArrayList<>();

		for (int i = 0; i < 4; i++) {
			movieEntityList.add(getTestMovieEntity(i));
		}

		return movieEntityList;

	}

}
