package com.alexarraiza.domain.common;

import io.reactivex.Observable;
import io.reactivex.ObservableSource;

public class TestTransformer<T> extends Transformer<T> {
	@Override
	public ObservableSource<T> apply(Observable<T> upstream) {
		return upstream;
	}
}
