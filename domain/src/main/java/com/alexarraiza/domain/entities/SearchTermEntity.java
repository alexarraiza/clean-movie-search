package com.alexarraiza.domain.entities;

import java.util.Date;

public class SearchTermEntity {

	private int id;
	private String searchTerm;
	private Date searchDate;

	public SearchTermEntity() {
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getSearchTerm() {
		return searchTerm;
	}

	public void setSearchTerm(String searchTerm) {
		this.searchTerm = searchTerm;
	}

	public Date getSearchDate() {
		return searchDate;
	}

	public void setSearchDate(Date searchDate) {
		this.searchDate = searchDate;
	}
}
