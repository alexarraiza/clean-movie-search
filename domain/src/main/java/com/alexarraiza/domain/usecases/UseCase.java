package com.alexarraiza.domain.usecases;

import com.alexarraiza.domain.common.Transformer;

import java.util.Map;

import io.reactivex.Observable;

public abstract class UseCase<T> {

	private Transformer<T> transformer;

	public UseCase(Transformer<T> transformer) {
		this.transformer = transformer;
	}

	abstract Observable<T> createObservable(Map<String, ?> data);

	Observable<T> observable(Map<String, ?> withData){
		return createObservable(withData).compose(transformer);
	}

}
