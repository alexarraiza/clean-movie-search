package com.alexarraiza.domain.usecases;

import com.alexarraiza.domain.MoviesRepository;
import com.alexarraiza.domain.common.Transformer;
import com.alexarraiza.domain.entities.MovieEntity;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import io.reactivex.Observable;

public class SearchMovieUseCase extends UseCase<List<MovieEntity>> {

	public static final String DATA_KEY_SEARCH_QUERY = "DATA_KEY_SEARCH_QUERY";

	private MoviesRepository moviesRepository;

	public SearchMovieUseCase(Transformer<List<MovieEntity>> transformer, MoviesRepository moviesRepository) {
		super(transformer);
		this.moviesRepository = moviesRepository;
	}

	public Observable<List<MovieEntity>> search(String query) {
		Map<String, String> data = new HashMap<>();
		data.put(DATA_KEY_SEARCH_QUERY, query);
		return observable(data);
	}

	@Override
	Observable<List<MovieEntity>> createObservable(Map<String, ?> data) {
		return moviesRepository.searchMovie(data.get(DATA_KEY_SEARCH_QUERY).toString());
	}
}
