package com.alexarraiza.domain;

import com.alexarraiza.domain.entities.MovieEntity;

import java.util.List;

import io.reactivex.Observable;

public interface MoviesRepository {

	Observable<List<MovieEntity>> searchMovie(String searchQuery);

}
