package com.alexarraiza.domain.usecases;

import com.alexarraiza.domain.MoviesRepository;
import com.alexarraiza.domain.common.TestTransformer;
import com.alexarraiza.domain.entities.MovieEntity;

import org.junit.Test;
import org.mockito.Mockito;

import java.util.List;

import io.reactivex.Observable;

import static com.alexarraiza.domain.common.DomainTestUtils.generateMovieEntityList;

public class SearchMovieUseCaseTest {

	private static final String SEARCH_QUERY = "test query";

	@Test
	public void searchMovies() {
		MoviesRepository moviesRepository = Mockito.mock(MoviesRepository.class);

		SearchMovieUseCase searchMovieUseCase = new SearchMovieUseCase(new TestTransformer<List<MovieEntity>>(), moviesRepository);

		List<MovieEntity> movieEntityList = generateMovieEntityList();

		Mockito.when(moviesRepository.searchMovie(SEARCH_QUERY)).thenReturn(
				Observable.just(movieEntityList)
		);

		searchMovieUseCase.search(SEARCH_QUERY).test()
				.assertComplete()
				.assertValue(movieEntityList);
	}

}